package com.namsi.eimyflipboard.util

import kotlin.math.min

fun maxAreaHistogram(row: MutableList<Int>): MutableMap<String, Int> {

    var maxArea = 0
    var maxAnchor = Pair(0, 0)
    val pStack = mutableListOf<Int>()
    val hStack = mutableListOf<Int>()

    for (i in 0 until row.size) {
        var lastWidth = row.size + 1

        while (hStack.isNotEmpty() && hStack.last() > row[i]) {
            lastWidth = pStack.last()
            val area = (i - pStack.last()) * hStack.last()
            if (area > maxArea) {
                maxArea = area
                maxAnchor = Pair(pStack.last(), hStack.last())
            }
            pStack.removeLast()
            hStack.removeLast()
        }

        if (hStack.isEmpty() || hStack.last() < row[i]) {
            if (row[i] != 0){
                pStack.add(min(lastWidth, i))
                hStack.add(row[i])
            }
        }
    }

    while (hStack.isNotEmpty()) {
        val area = (row.size - pStack.last()) * hStack.last()
        if (area > maxArea) {
            maxArea = area
            maxAnchor = Pair(pStack.last(), hStack.last())
        }
        pStack.removeLast()
        hStack.removeLast()
    }

    return mutableMapOf("maxArea" to maxArea, "yAnchor" to maxAnchor.first, "heightAnchor" to maxAnchor.second)
}

fun maxRectangle(matrix: MutableList<MutableList<Int>>): MutableMap<String, Int> {
    val numColumns = matrix.size
    val numRows = matrix[0].size

    var result = maxAreaHistogram(matrix[0])
    result["xAnchor"] = 0

    for (i in 1 until numRows) {
        for (j in 0 until numColumns)  // if A[i][j] is 1 then add A[i -1][j]
            if (matrix[i][j] == 1) matrix[i][j] += matrix[i - 1][j]

        val newRowResult = maxAreaHistogram(matrix[i])

        if ((result["maxArea"] as Int) < (newRowResult["maxArea"] as Int)) {
            result = newRowResult
            result["xAnchor"] = i
        }
    }

    return result
}
