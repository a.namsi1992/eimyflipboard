package com.namsi.eimyflipboard

data class BoardItem(
    var isSelected: Boolean = false,
    var isInRectangle: Boolean = false,
    var isInMaxRectangle: Boolean = false,
    var value: Int = 0
)
