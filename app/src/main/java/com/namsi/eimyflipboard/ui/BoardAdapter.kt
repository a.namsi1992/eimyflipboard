package com.namsi.eimyflipboard.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.namsi.eimyflipboard.BoardItem
import com.namsi.eimyflipboard.databinding.BoardItemBinding

class BoardAdapter(
    private val boardList: MutableList<MutableList<BoardItem>>,
    private val clickListener: OnItemClickListener
) :
    RecyclerView.Adapter<BoardViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BoardViewHolder {
        val itemBinding = BoardItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return BoardViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: BoardViewHolder, position: Int) {
        val item = boardList[position.mod(boardList[0].size)][position.div(boardList[0].size)]
        holder.bind(item, clickListener, holder.itemView.context)
    }

    override fun getItemCount(): Int = boardList.size * boardList[0].size
}