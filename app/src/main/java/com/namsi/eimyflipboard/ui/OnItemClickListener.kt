package com.namsi.eimyflipboard.ui

interface OnItemClickListener {
    fun onItemClick(position: Int)
}