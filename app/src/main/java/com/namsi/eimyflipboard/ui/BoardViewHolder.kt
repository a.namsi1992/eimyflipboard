package com.namsi.eimyflipboard.ui

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import com.namsi.eimyflipboard.BoardItem
import com.namsi.eimyflipboard.R
import com.namsi.eimyflipboard.databinding.BoardItemBinding

class BoardViewHolder(private val binding: BoardItemBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(item: BoardItem, clickListener: OnItemClickListener, context: Context) {
        binding.itemSpace.setBackgroundColor(
            context.getColor(
                when {
                    item.isSelected -> R.color.selected_color
                    item.isInRectangle -> R.color.marked_color
                    item.isInMaxRectangle -> R.color.max_color
                    else -> R.color.unselected_color
                }
            )
        )

        binding.itemSpace.setOnClickListener {
            clickListener.onItemClick(adapterPosition)
        }
    }
}