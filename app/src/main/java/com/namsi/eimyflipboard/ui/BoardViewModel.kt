package com.namsi.eimyflipboard.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.namsi.eimyflipboard.BoardItem
import com.namsi.eimyflipboard.util.maxRectangle
import kotlin.math.ceil

class BoardViewModel : ViewModel() {

    val maxPossibleLiveData = MutableLiveData<Int>(0)
    val maxSelectedLiveData = MutableLiveData<Int>(0)

    var maxRectangleArea = 0
    var maxSelctedArea = 0
    var size = 15
    var board = MutableList(size) { MutableList(size) { BoardItem() } }
    var selectedItems = 0

    fun clickItem(position: Int) {

        val clickedItem = board[position.mod(size)][position.div(size)]
        if (clickedItem.isSelected) {
            clickedItem.isSelected = false
            selectedItems--
        } else {
            clickedItem.isSelected = true
            selectedItems++
        }

        resetMarkedBoard()
        val intBoard = board.map { row -> row.map { if (it.isSelected) 0 else 1 } } as MutableList<MutableList<Int>>
        setMaxRectangle(maxRectangle(intBoard))
    }


    private fun resetMarkedBoard() {
        for (row in board)
            for (item in row) {
                item.isInRectangle = false
                item.isInMaxRectangle = false
            }
    }

    private fun setMaxRectangle(maxRectangle: MutableMap<String, Int>) {
        maxRectangleArea = maxRectangle["maxArea"] as Int
        maxPossibleLiveData.value = maxRectangleArea

        val xAnchor = maxRectangle["xAnchor"]
        val yAnchor = maxRectangle["yAnchor"]
        val height = maxRectangle["heightAnchor"]
        if (xAnchor != null && yAnchor != null && height != null) {
            val nbRows = maxRectangleArea / height

            for (i in xAnchor downTo xAnchor - height + 1)
                for (j in yAnchor until (yAnchor + nbRows)) {
                    board[i][j].isInMaxRectangle = true
                }


            if (maxRectangleArea >= selectedItems) {
                maxSelctedArea = selectedItems
                maxSelectedLiveData.value = maxSelctedArea

                setRectangle(nbRows, height, xAnchor, yAnchor)
            } else {
                maxSelectedLiveData.value = -1
            }
        }

    }

    private fun setRectangle(maxHeight: Int, maxWidth: Int, xAnchor: Int, yAnchor: Int) {

        var isHeight = false
        var minReste = selectedItems
        var maxDivider = minOf(selectedItems, maxHeight, maxWidth)
        if (maxDivider == maxHeight) isHeight = true
        val maxI = maxDivider / 2 + 2

        for (i in 1 until maxI) {
            val d = selectedItems % i
            if (d <= minReste) {
                if (isHeight && selectedItems / i <= maxHeight) {
                    maxDivider = i
                    minReste = d
                } else if (!isHeight && selectedItems / i <= maxWidth) {
                    maxDivider = i
                    minReste = d
                }
            }
        }
        val nbRows = if (isHeight) maxDivider else ceil(selectedItems.toDouble() / maxDivider)
        val nbCols = if (!isHeight) maxDivider else ceil(selectedItems.toDouble() / maxDivider)

        var itemsCounter = selectedItems
        for (i in xAnchor downTo xAnchor - nbCols.toInt())
            for (j in yAnchor until yAnchor + nbRows.toInt()) {
                if (itemsCounter != 0) {
                    board[i][j].isInRectangle = true
                    itemsCounter--
                }
            }

    }
}