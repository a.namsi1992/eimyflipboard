package com.namsi.eimyflipboard.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.namsi.eimyflipboard.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    private val viewModel: BoardViewModel by viewModels()
    private lateinit var boardAdapter: BoardAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        initObservers()
        val boardClickListener: OnItemClickListener = object : OnItemClickListener {
            override fun onItemClick(position: Int) {
                viewModel.clickItem(position)
                boardAdapter.notifyDataSetChanged()
            }
        }
        boardAdapter = BoardAdapter(viewModel.board, boardClickListener)
        binding.boardRecycler.layoutManager = GridLayoutManager(this, viewModel.size)
        binding.boardRecycler.adapter = boardAdapter

    }

    @SuppressLint("NotifyDataSetChanged")
    private fun initObservers() {
        viewModel.maxPossibleLiveData.observe(this, {
            binding.maxNb.text = it.toString()
            boardAdapter.notifyDataSetChanged()
        })
        viewModel.maxSelectedLiveData.observe(this, {
            if (it == -1) {
                binding.errorMsg.visibility = View.VISIBLE
                binding.markedNb.text = ""
            } else {
                binding.markedNb.text = it.toString()
                boardAdapter.notifyDataSetChanged()
                binding.errorMsg.visibility = View.GONE
            }
        })
    }
}
